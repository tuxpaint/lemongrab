package lemongrab

import "context"

type Backend interface {
	// if unlock != nil, then you acquired leadership
	TryLock(ctx context.Context) (unlock func() error, err error)
}
